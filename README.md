# receipt-storage-batch

An application to insert receipt information on fixed scheduled basis (daily, bi-weekly, montly) invoking the REST interface of  
[receipt-storage-svc](https://bitbucket.org/zafirotech/receipt-storage-svc).

It doesn't handle any authentication so it's intended to be used in a local interface only.ts handling would happen there.

## Prerequisites

You will need [Python3](https://www.python.org/downloads/) 


## Running

Setup environment "RECEIPT_STORAGE_BATCH_HOME" to the receipt-storage-batch location and run with python3 receipt_storage_batch.py

it will match the current date with the preconfigured entries in config file config/receipt-storage-config.json.

```json
{"receipt-entries": [
           {
              "title": "Sample Transaction #1",
              "description": "Monthly sample every 3rd day",
              "amount": 200,
              "freq": "M03"
            },
            {
               "title": "Sam[ple Transaction #2",
               "description": "Bi-weekly sample starting on Oct 11",
               "amount": 210,
               "freq": "BW20171011"
             }]}
```

## Related projects

+ [receipt-storage-svc](https://bitbucket.org/zafirotech/receipt-storage-svc) Main receipt storage project. Check it out...


## License

GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

