import os
import unittest
from datetime import datetime, timedelta

import mock

from receipt_storage_batch import receipt_storage_batch


class TestHelperMethods(unittest.TestCase):

    def setUp(self):
        receipt_storage_batch.JSON_CONFIG = os.path.join(os.path.dirname(__file__), 'resources/receipt-storage-config.json')

    def test_daily_entry(self):
        self.assertTrue(receipt_storage_batch.matches_with_today('D'))

    def test_valid_monthly_entry(self):
        today = datetime.now()
        self.assertTrue(receipt_storage_batch.matches_with_today('M' + str(today.day)))

    def test_invalid_monthly_entry(self):
        yesterday = datetime.now() - timedelta(days=1)
        tomorrow = datetime.now() + timedelta(days=1)
        self.assertFalse(receipt_storage_batch.matches_with_today('M' + str(yesterday.day)))
        self.assertFalse(receipt_storage_batch.matches_with_today('M' + str(tomorrow.day)))

    def test_valid_biweekly_entry(self):
        two_weeks_ago = datetime.now() - timedelta(days=14)
        self.assertTrue(receipt_storage_batch.matches_with_today('B' + two_weeks_ago.strftime(receipt_storage_batch.CONST_DATE_FORMAT)))

    def test_invalid_biweekly_entry(self):
        over_two_weeks_ago = datetime.now() - timedelta(days=15)
        below_two_weeks_ago = datetime.now() - timedelta(days=13)
        self.assertFalse(receipt_storage_batch.matches_with_today('B' + over_two_weeks_ago.strftime(receipt_storage_batch.CONST_DATE_FORMAT)))
        self.assertFalse(receipt_storage_batch.matches_with_today('B' + below_two_weeks_ago.strftime(receipt_storage_batch.CONST_DATE_FORMAT)))

    def test_load_receipt_entries(self):
        receipt_entries = receipt_storage_batch.load_receipt_entries()
        self.assertEqual(len(receipt_entries),  2)

    @mock.patch('requests.post',  return_value= "FILE_UPLOAD_NO-ADDRECORD_OK")
    def test_post_entry(self, mock_post):
        receipt_entry = {"title": "Car Insurance", "description": "Car Insurance", "amount": 200 }
        result = receipt_storage_batch.post_entry(receipt_entry)
        self.assertEqual(mock_post.call_args[0], (receipt_storage_batch.CONST_RECEIPT_SVC_URL,))
        self.assertEqual(mock_post.call_args[1], {'data': {'title': 'Car Insurance', 'description': 'Car Insurance', 'amount': 200, 'date': datetime.now().date().strftime(receipt_storage_batch.CONST_DATE_FORMAT), 'category': 'PERSONAL'}})
        self.assertEqual(result, "FILE_UPLOAD_NO-ADDRECORD_OK")