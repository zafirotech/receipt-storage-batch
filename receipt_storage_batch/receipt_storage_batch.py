from datetime import datetime
import json
import requests
import os
import logging

CONST_RECEIPT_SVC_URL = 'http://localhost:3000/receipt-storage-svc/store'
CONST_DATE_FORMAT = '%Y-%m-%d'

JSON_CONFIG = 'config/config/receipt-storage-config.json'
LOG_FILE = 'receipt-storage-batch.log'

if 'RECEIPT_STORAGE_BATCH_HOME' in os.environ:
    JSON_CONFIG = os.path.join(os.environ['RECEIPT_STORAGE_BATCH_HOME'], 'config/receipt-storage-config.json')
    LOG_FILE = os.path.join(os.environ['RECEIPT_STORAGE_BATCH_HOME'], 'logs/receipt-storage-batch.log')


logger = logging.getLogger("receipt-storage-batch")
logger.setLevel(logging.INFO)
# create file handler which logs even debug messages
fh = logging.FileHandler(LOG_FILE)
fh.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)


class DailyMatcher:
    def matches(self, freq):
        return True


class MonthlyMatcher:
    def matches(self, freq):
        today = datetime.now()
        return today.day == int(freq)


class BiWeeklyMatcher:
    def matches(self, freq):
        ref_date = datetime.strptime(freq, CONST_DATE_FORMAT).date()
        today = datetime.now().date()
        number_of_days = (today - ref_date).days
        return number_of_days % 14 == 0


CONST_MATCHERS = {"D": DailyMatcher(), "M": MonthlyMatcher(), "B": BiWeeklyMatcher()}

def matches_with_today(freq):
    """
     To validate a specific own crontrab implementation. So far, the followings example:
      D -> to be run daily, always matches with today
      M5 -> matches every fifth day of the month
      B2017-10-11 -> matches bi-weekly using the supplied date (yyyy-MM-dd) as the first date to match
    """
    return CONST_MATCHERS[freq[0]].matches(freq[1:])


def load_receipt_entries():
    """
     Load receipts information from JSON File into Python data structure
    """
    try:
        with open(JSON_CONFIG) as data_file:
            return json.load(data_file)['receipt-entries']
    except FileNotFoundError:
        print("""
        Configuration JSON File has not been setup. RECEIPT_STORAGE_BATCH_HOME system variable
        should point to a json config file.
        """)


def post_entry(receipt_entry):
    """
     Perform POST to the receipt service. The automatic post doesn't require file upload and it will be only
     in the PERSONAL category.
    """
    receipt_data = receipt_entry
    receipt_data['date'] = datetime.now().date().strftime(CONST_DATE_FORMAT)
    receipt_data['category'] = 'PERSONAL'
    r = requests.post(CONST_RECEIPT_SVC_URL, data=receipt_data)
    return r


def process_entries():
    for receipt_entry in load_receipt_entries():
        if matches_with_today(receipt_entry['freq']):
            r = post_entry(receipt_entry)
            logger.info("Post Http Response:{}, Passed Parameters:{}".format(r, receipt_entry))


if __name__ == "__main__":
    process_entries()
